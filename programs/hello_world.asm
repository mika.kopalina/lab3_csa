section .interrupts
interrupt_vector_0:    
    JMP interrupt_handler

section .data
hello_world:
    cstr "Hello, World!\n"  ; Null-terminated C string

section .text
    _start:
    DI
    MOV r1 hello_world   ; Загрузка адреса строки в регистр r1
    LD r2 r1             ; Загрузка первого символа строки в регистр r2
    MOV r3 0             ; Запись нуля в регистр r3

print_str_loop:
    ST OUT r2            ; Вывод символа из регистра r2
    INC r1                ; Переход к следующему символу в строке
    LD r2 r1             ; Загрузка следующего символа строки в регистр r2
    CMP r2 r3            ; Сравнение значения в регистре r2 с нулевым регистром r3
    JZ print_str_end      ; Если значение в r2 равно нулю завершить цикл
    JMP print_str_loop    

print_str_end:
    HALT

interrupt_handler:
    RET
