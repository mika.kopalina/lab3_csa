section .interrupts 
interrupt_vector_0:
    JMP interrupt_handler

section .data
section .text
_start:
    DI
    MOV r5 '\n'     
    EI               ; Включаем прерывания
main_loop:
    JMP main_loop    ; Бесконечный цикл

interrupt_handler:
    DI
    LD r1 IN        ; Загрузка символа из входного порта
    ST OUT r1       ; Выводим символ в выходной порт
    CMP r1 r5
    JZ end
    EI
    RET      
end:
    HALT   