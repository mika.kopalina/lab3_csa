section .interrupts
interrupt_vector_0:
    JMP interrupt_handler

section .data
prompt_msg:
    cstr "What is your name?"   ; Нуль-терминированное сообщение вопроса
hello_msg: 
    cstr "Hello, "              ; Нуль-терминированное приветствие
user_name:
    times 20 word 0            ; Создание массива из 255 байтов заполненных 0
word_complete:
    word 0                      ; Флаг для указания завершения слова инициализированный нулем
        
section .text
_start:
    MOV r6 user_name            ; Записываем в r6 адрес начала буфера ввода данных
                                ; Значение r6 будем изменяться только в обработчике прерываний
    EI                          
    ; Вывод вопроса
    MOV r1 prompt_msg
    CALL print_str
    MOV r1 '\n'
    ST OUT r1

    ; Ожидаем окончания ввода данных
    MOV r2 0
spin_loop:
    EI
    LD r1 word_complete
    DI
    CMP r1 r2
    JZ spin_loop
    EI
      
    ; Вывод приветствия
    MOV r1 hello_msg
    CALL print_str
    MOV r1 user_name
    CALL print_str
          
    DI
    HALT

print_str:
; Печать строки завершенной нулем указанной в регистре r1
print_str_loop:
    EI
    LD r2 r1            ; Загрузка символа из памяти по адресу хранящемуся в регистре r1
    MOV r3 0
    DI
    CMP r2 r3           ; Проверка на 0
    JZ print_str_end     ; Если встречен 0 завершить цикл
    EI
    ST OUT r2           ; Вывод символа
    INC r1               ; Переход к следующему символу
    JMP print_str_loop   
print_str_end:
    EI
    RET

; Обработчик прерывания
interrupt_handler:
    DI                   ; Выключение прерываний
    LD r4 IN            ; Загрузка в r1 данных из устройства ввода
    ST r6 r4            ; Загрузка данных из r1 в ячейку памяти по адресу хранящемуся в r6
    INC r6               ; Передвигаем указатель буффера ввода вперед на 1
    MOV r5 '\n'            
    CMP r4 r5           ; Сравниваем r1 с '\n'
    JNZ not_end
    MOV r5 1            ; Если '\n'
    ST word_complete r5 ; Устанавливаем флаг окончания чтения слова
    EI                   ; Включение прерываний
    RET                  ; Возврат из прерывания                 
not_end:                 ; Если не '\n'
    EI                  ; Включение прерываний
    RET                 ; Возврат из прерывания