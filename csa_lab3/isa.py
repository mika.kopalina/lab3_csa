from __future__ import annotations

import enum


@enum.unique
class ArgType(enum.Flag):
    REG = enum.auto()
    INT = enum.auto()


class Argument:
    def __init__(self, arg_type: ArgType, value: int) -> None:
        self.arg_type = arg_type
        self.value = value


@enum.unique
class Opcode(enum.Enum):
    INC = enum.auto()
    DEC = enum.auto()
    HALT = enum.auto()
    NOP = enum.auto()
    EI = enum.auto()
    DI = enum.auto()
    MOV = enum.auto()
    LD = enum.auto()
    ST = enum.auto()
    ADD = enum.auto()
    SUB = enum.auto()
    MUL = enum.auto()
    DIV = enum.auto()
    REM = enum.auto()
    CMP = enum.auto()
    JMP = enum.auto()
    JG = enum.auto()
    JZ = enum.auto()
    JNZ = enum.auto()
    PUSH = enum.auto()
    POP = enum.auto()
    CALL = enum.auto()
    RET = enum.auto()


class Instruction:
    def __init__(self, opcode: Opcode, args: list[Argument]) -> None:
        self.opcode = opcode
        self.args = args

    def __str__(self) -> str:
        res = f"{self.opcode.name:<4} "
        for arg in self.args:
            res += f"{arg.arg_type.name:<4} {hex(arg.value):<10} "
        return res

    @staticmethod
    def parse(line: str) -> Instruction:
        words = line.split()
        return Instruction(
            Opcode[words[0]], [Argument(ArgType[x], int(y, 16)) for x, y in zip(words[1::2], words[2::2])]
        )
