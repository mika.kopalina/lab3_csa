from __future__ import annotations

import enum
import logging

import pytest

from csa_lab3.isa import ArgType, Argument, Instruction, Opcode
from csa_lab3.machine.datapath import AluOp, Datapath, DatapathOut, DataRegisterIn, DecOrIncMode, InputIReg
from csa_lab3.machine.wire import Latch, Mux, Signal, Wire
from csa_lab3.mem_layout import INTERRUPT_VECTOR_0, SP_ID, TEXT_START


class InterruptController:
    def __init__(self, input_int: Wire) -> None:
        self.EI: bool = False

        self.ints: int = 0
        input_int.subscribe(self.consume_input_int)

    def consume_input_int(self, new_sig: Signal) -> None:
        self.ints += 1

    def hadle_int(self) -> None:
        assert self.has_int()
        self.ints = max(self.ints - 1, 0)

    def has_int(self) -> bool:
        return self.EI and self.ints > 0

    def set_di(self) -> None:
        self.EI = False

    def set_ei(self) -> None:
        self.EI = True


@enum.unique
class ProgramCounter(enum.Enum):
    PC_INC = enum.auto()
    CONTROL_UNIT_ARG = enum.auto()


@enum.unique
class ControlUnitArg(enum.Enum):
    PROGRAM_CNT = enum.auto()
    SELECT_ARG = enum.auto()
    DATAPATH_OUT = enum.auto()


class ControlUnit:
    def __init__(self, initial_layout: dict[int, Signal], input_buffer: list[tuple[int, str]]):
        self.tick: int = 0
        self.clock: Wire = Wire()

        control_unit_arg_wire: Wire = Wire()
        pc_wire: Wire = Wire()
        self.datapath = Datapath(
            control_unit_arg_wire, control_unit_arg_wire, pc_wire, initial_layout, input_buffer, self.clock
        )
        self.int_ctrl = InterruptController(self.datapath.get_input_int())

        self.instr: Instruction = Instruction(Opcode.JMP, [Argument(ArgType.INT, TEXT_START)])
        self.datapath.get_instruction().subscribe(self.update_instruction)

        inc_pc_wire: Wire = Wire()
        self.pc_mux: Mux = Mux(
            [(ProgramCounter.PC_INC.value, inc_pc_wire), (ProgramCounter.CONTROL_UNIT_ARG.value, control_unit_arg_wire)]
        )

        self.PC: Latch = Latch(self.pc_mux.output, "PC")
        self.PC.output.subscribe(
            lambda new_sig: inc_pc_wire.set_signal(Signal(None if new_sig.value is None else new_sig.value + 1))
        )
        self.PC.output.subscribe(lambda new_sig: pc_wire.set_signal(new_sig))

        self.select_arg_wire: Wire = Wire()
        self.control_unit_arg_mux: Mux = Mux(
            [
                (ControlUnitArg.DATAPATH_OUT.value, self.datapath.get_datapath_out()),
                (ControlUnitArg.PROGRAM_CNT.value, self.PC.output),
                (ControlUnitArg.SELECT_ARG.value, self.select_arg_wire),
            ]
        )
        self.control_unit_arg_mux.output.subscribe(lambda x: control_unit_arg_wire.set_signal(x))

    def update_instruction(self, new_sig: Signal) -> None:
        assert new_sig.instr is not None
        self.instr = new_sig.instr

    def tick_inc(self) -> None:
        self.tick += 1
        self.clock.set_signal(Signal(self.tick))

    def fetch_next_instr(self) -> None:
        self.control_unit_arg_mux.set_sel(ControlUnitArg.PROGRAM_CNT.value)
        self.datapath.AR.latch()
        self.datapath.set_oe(True)
        self.datapath.IR.latch()
        self.datapath.set_oe(False)

        self.tick_inc()

    def set_control_unit_arg(self, arg: Argument) -> None:
        if arg.arg_type == ArgType.REG:
            self.datapath.register_file.set_sel_lreg(arg.value)
            self.datapath.alu.set_op(AluOp.PASS_L)
            self.datapath.datapath_out_mux.set_sel(DatapathOut.ALU_OUTPUT.value)
            self.control_unit_arg_mux.set_sel(ControlUnitArg.DATAPATH_OUT.value)
        else:
            self.select_arg_wire.set_signal(Signal(arg.value))
            self.control_unit_arg_mux.set_sel(ControlUnitArg.SELECT_ARG.value)

    def sp_inc_to_data_addr(self) -> None:
        self.datapath.register_file.set_sel_lreg(SP_ID)
        self.datapath.register_file.set_sel_ireg(SP_ID)
        self.datapath.alu.set_op(AluOp.INC_L)
        self.datapath.input_ireg_mux.set_sel(InputIReg.ALU_OUTPUT.value)
        self.datapath.datapath_out_mux.set_sel(DatapathOut.ALU_OUTPUT.value)
        self.control_unit_arg_mux.set_sel(ControlUnitArg.DATAPATH_OUT.value)
        self.datapath.AR.latch()
        self.datapath.register_file.latch_ireg()

    def decode_execute_instr(self, inc_pc: bool = True) -> None:
        self.pc_mux.set_sel(ProgramCounter.PC_INC.value)
        self.PC.latch() if inc_pc else None
        match self.instr.opcode:
            case Opcode.INC | Opcode.DEC:
                assert len(self.instr.args) == 1
                assert self.instr.args[0].arg_type == ArgType.REG
                self.datapath.register_file.set_sel_lreg(self.instr.args[0].value)
                self.datapath.dec_or_inc.set_mode(
                    DecOrIncMode.DEC if self.instr.opcode == Opcode.DEC else DecOrIncMode.INC
                )
                self.datapath.input_ireg_mux.set_sel(InputIReg.DEC_OR_INC.value)
                self.datapath.register_file.set_sel_ireg(self.instr.args[0].value)
                self.datapath.register_file.latch_ireg()
            case Opcode.PUSH:
                assert len(self.instr.args) == 1
                assert self.instr.args[0].arg_type == ArgType.REG
                self.sp_inc_to_data_addr()

                self.datapath.register_file.set_sel_lreg(self.instr.args[0].value)
                self.datapath.alu.set_op(AluOp.PASS_L)
                self.datapath.data_register_in_mux.set_sel(DataRegisterIn.ALU_OUTPUT.value)
                self.datapath.DR.latch()
                self.datapath.set_wr(True)
                self.datapath.set_wr(False)
            case Opcode.POP:
                assert len(self.instr.args) == 1
                assert self.instr.args[0].arg_type == ArgType.REG
                self.datapath.register_file.set_sel_lreg(SP_ID)
                # This part
                self.datapath.alu.set_op(AluOp.PASS_L)
                self.datapath.datapath_out_mux.set_sel(DatapathOut.ALU_OUTPUT.value)
                self.control_unit_arg_mux.set_sel(ControlUnitArg.DATAPATH_OUT.value)
                self.datapath.AR.latch()

                # And this part could be parallel
                self.datapath.dec_or_inc.set_mode(DecOrIncMode.DEC)
                self.datapath.input_ireg_mux.set_sel(InputIReg.DEC_OR_INC.value)
                self.datapath.register_file.set_sel_ireg(SP_ID)

                # This part
                self.datapath.register_file.latch_ireg()
                self.datapath.register_file.set_sel_ireg(self.instr.args[0].value)
                self.datapath.input_ireg_mux.set_sel(InputIReg.DATA_REGISTER.value)

                # Could be parallel with this part
                self.datapath.data_register_in_mux.set_sel(DataRegisterIn.DATA_OUT.value)
                self.datapath.set_oe(True)
                self.datapath.DR.latch()
                self.datapath.set_oe(False)

                self.datapath.register_file.latch_ireg()
            case Opcode.RET:
                self.datapath.register_file.set_sel_lreg(SP_ID)
                self.datapath.alu.set_op(AluOp.PASS_L)
                self.datapath.datapath_out_mux.set_sel(DatapathOut.ALU_OUTPUT.value)
                self.control_unit_arg_mux.set_sel(ControlUnitArg.DATAPATH_OUT.value)
                self.datapath.AR.latch()

                # This part
                self.datapath.dec_or_inc.set_mode(DecOrIncMode.DEC)
                self.datapath.input_ireg_mux.set_sel(InputIReg.DEC_OR_INC.value)
                self.datapath.register_file.set_sel_ireg(SP_ID)
                self.datapath.register_file.latch_ireg()

                # And this part
                self.datapath.data_register_in_mux.set_sel(DataRegisterIn.DATA_OUT.value)
                self.datapath.set_oe(True)
                self.datapath.DR.latch()
                self.datapath.set_oe(False)

                # And this part could be parallel
                self.datapath.datapath_out_mux.set_sel(DatapathOut.DATA_REGISTER.value)
                self.pc_mux.set_sel(ProgramCounter.CONTROL_UNIT_ARG.value)

                self.PC.latch()
            case Opcode.CALL:
                assert len(self.instr.args) == 1
                assert self.instr.args[0].arg_type == ArgType.INT
                # This part
                self.datapath.data_register_in_mux.set_sel(DataRegisterIn.PROGRAM_COUNT.value)
                self.datapath.DR.latch()
                self.select_arg_wire.set_signal(Signal(self.instr.args[0].value))
                self.control_unit_arg_mux.set_sel(ControlUnitArg.SELECT_ARG.value)
                self.pc_mux.set_sel(ProgramCounter.CONTROL_UNIT_ARG.value)
                self.PC.latch()

                # And this part could be parallel
                self.sp_inc_to_data_addr()

                self.datapath.set_wr(True)
                self.datapath.set_wr(False)

            case Opcode.HALT:
                raise StopIteration()
            case Opcode.NOP:
                pass
            case Opcode.EI:
                self.int_ctrl.set_ei()
            case Opcode.DI:
                self.int_ctrl.set_di()
            case Opcode.MOV:
                assert len(self.instr.args) == 2
                assert self.instr.args[0].arg_type == ArgType.REG
                self.datapath.register_file.set_sel_ireg(self.instr.args[0].value)
                self.set_control_unit_arg(self.instr.args[1])
                self.datapath.input_ireg_mux.set_sel(InputIReg.CONTROL_UNIT_ARG.value)
                self.datapath.register_file.latch_ireg()
            case Opcode.LD:
                assert len(self.instr.args) == 2
                assert self.instr.args[0].arg_type == ArgType.REG
                self.set_control_unit_arg(self.instr.args[1])
                self.datapath.AR.latch()

                self.datapath.data_register_in_mux.set_sel(DataRegisterIn.DATA_OUT.value)
                self.datapath.set_oe(True)
                self.datapath.DR.latch()
                self.datapath.set_oe(False)

                self.datapath.input_ireg_mux.set_sel(InputIReg.DATA_REGISTER.value)
                self.datapath.register_file.set_sel_ireg(self.instr.args[0].value)
                self.datapath.register_file.latch_ireg()
            case Opcode.ST:
                assert len(self.instr.args) == 2
                assert self.instr.args[1].arg_type == ArgType.REG
                self.set_control_unit_arg(self.instr.args[0])
                self.datapath.AR.latch()
                self.datapath.register_file.set_sel_lreg(self.instr.args[1].value)
                self.datapath.alu.set_op(AluOp.PASS_L)
                self.datapath.data_register_in_mux.set_sel(DataRegisterIn.ALU_OUTPUT.value)
                self.datapath.DR.latch()
                self.datapath.set_wr(True)
                self.datapath.set_wr(False)
            case Opcode.ADD | Opcode.SUB | Opcode.MUL | Opcode.DIV | Opcode.REM:
                assert len(self.instr.args) == 3
                assert all(map(lambda x: x.arg_type == ArgType.REG, self.instr.args))
                self.datapath.input_ireg_mux.set_sel(InputIReg.ALU_OUTPUT.value)
                self.datapath.register_file.set_sel_ireg(self.instr.args[0].value)
                self.datapath.register_file.set_sel_lreg(self.instr.args[1].value)
                self.datapath.register_file.set_sel_rreg(self.instr.args[2].value)
                self.datapath.alu.set_set_nz(True)
                self.datapath.alu.set_op(AluOp[self.instr.opcode.name])
                self.datapath.register_file.latch_ireg()
            case Opcode.CMP:
                assert len(self.instr.args) == 2
                assert all(map(lambda x: x.arg_type == ArgType.REG, self.instr.args))
                self.datapath.register_file.set_sel_lreg(self.instr.args[0].value)
                self.datapath.register_file.set_sel_rreg(self.instr.args[1].value)
                self.datapath.alu.set_set_nz(True)
                self.datapath.alu.set_op(AluOp[self.instr.opcode.name])
            case Opcode.JMP | Opcode.JG | Opcode.JZ | Opcode.JNZ:
                assert len(self.instr.args) == 1
                jmp: bool = False
                if self.instr.opcode == Opcode.JG and (not self.datapath.alu.N and not self.datapath.alu.Z):
                    jmp = True
                if self.instr.opcode == Opcode.JZ and self.datapath.alu.Z:
                    jmp = True
                if self.instr.opcode == Opcode.JNZ and (not self.datapath.alu.Z):
                    jmp = True
                if self.instr.opcode == Opcode.JMP:
                    jmp = True
                if jmp:
                    self.set_control_unit_arg(self.instr.args[0])
                    self.pc_mux.set_sel(ProgramCounter.CONTROL_UNIT_ARG.value)
                    self.PC.latch()
            case _:
                pytest.fail("Unknown opcode")
        self.tick_inc()

    def check_int(self) -> None:
        if self.int_ctrl.has_int():
            logging.info("INTERRUPT DETECTED")
            self.int_ctrl.hadle_int()
            self.int_ctrl.set_di()
            self.instr = Instruction(Opcode.CALL, [Argument(ArgType.INT, INTERRUPT_VECTOR_0)])
            self.decode_execute_instr(False)

    def cmd(self) -> None:
        logging.info(f"{self}")
        self.decode_execute_instr()
        self.check_int()
        self.fetch_next_instr()

    def __str__(self) -> str:
        res = f"TICK: {self.tick:5} PC: {self.PC.value:>8}\n"
        res += f"CMD: [{self.instr}]\n"
        for k, reg in enumerate(self.datapath.register_file.regs):
            if reg is not None and (reg.value is not None or reg.instr is not None):
                res += f" R{k}: {reg:>8}\n"
        res += f" AR: {self.datapath.AR.value:>8} N|Z: {int(self.datapath.alu.N)}|{int(self.datapath.alu.Z)}"
        res += f" INT_CTRL: {self.int_ctrl.ints:>3} EI: {self.int_ctrl.EI}"
        return res
