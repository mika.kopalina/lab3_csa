from __future__ import annotations

from typing import Callable

from csa_lab3.isa import Instruction


class Signal:
    def __init__(self, value: int | None, instr: Instruction | None = None) -> None:
        self.value: int | None = value
        self.instr: Instruction | None = instr

    def __str__(self) -> str:
        if self.instr is not None:
            return f"[{self.instr.opcode:>8}]"
        if self.value is not None:
            return f"{hex(self.value):>8}"
        return "None"

    def __format__(self, format_spec):
        return format(str(self), format_spec)


class Wire:
    def __init__(self) -> None:
        self.callbacks: list[Callable[[Signal], None]] = []

    def subscribe(self, callback: Callable[[Signal], None]) -> None:
        self.callbacks.append(callback)

    def set_signal(self, signal: Signal) -> None:
        for cb in self.callbacks:
            cb(signal)


class WireUnion:
    def __init__(self, lhs: Wire, rhs: Wire) -> None:
        self.output: Wire = Wire()

        self.lhs: Signal = Signal(None)
        self.rhs: Signal = Signal(None)

        lhs.subscribe(self.update_lhs)
        rhs.subscribe(self.update_rhs)

    def update_lhs(self, new_sig: Signal) -> None:
        self.lhs = new_sig
        self.update()

    def update_rhs(self, new_sig: Signal) -> None:
        self.rhs = new_sig
        self.update()

    def update(self) -> None:
        assert (self.lhs.value is None and self.lhs.instr is None) or (
            self.rhs.value is None and self.rhs.instr is None
        )
        if self.lhs.value is None and self.lhs.instr is None:
            self.output.set_signal(self.rhs)
        if self.rhs.value is None and self.rhs.instr is None:
            self.output.set_signal(self.lhs)


class Mux:
    def __init__(self, wires: list[tuple[int, Wire]]) -> None:
        self.sel: int = 0
        self.values: list[Signal] = []

        for _ in range(len(wires) + 2):
            self.values.append(Signal(0))

        for wire_id, wire in wires:
            # type ignored due to https://github.com/python/mypy/issues/15459
            wire.subscribe(lambda x, wire_id=wire_id: self.update_signal(wire_id, x))  # type: ignore
        self.output: Wire = Wire()

    def update_signal(self, wire_id, new_sig: Signal) -> None:
        self.values[wire_id] = new_sig
        self.update()

    def update(self) -> None:
        self.output.set_signal(self.values[self.sel])

    def set_sel(self, sel: int) -> None:
        self.sel = sel
        self.update()


class Latch:
    def __init__(self, inp: Wire, latch_name: str) -> None:
        self.latch_name = latch_name
        self.sig: Signal = Signal(None)
        inp.subscribe(self.update)
        self.value: Signal = Signal(None)
        self.output = Wire()

    def update(self, new_sig: Signal) -> None:
        self.sig = new_sig

    def latch(self) -> None:
        self.value = self.sig
        self.output.set_signal(self.sig)
