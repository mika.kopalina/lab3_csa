from __future__ import annotations

import enum
import logging

import pytest

from csa_lab3.machine.wire import Latch, Mux, Signal, Wire, WireUnion
from csa_lab3.mem_layout import IN_ADDR, OUT_ADDR, SP_ID, STACK_START


class Memory:
    def __init__(self, initial_layout: dict[int, Signal], data_in: Wire, addr_in: Wire) -> None:
        self.data: dict[int, Signal] = initial_layout

        self.WR: bool = False  # Write
        self.OE: bool = False  # Output Enable
        self.CS: bool = False  # Chip Select

        self.data_in: Signal = Signal(None)
        data_in.subscribe(self.update_data_in)

        self.addr_in: Signal = Signal(None)
        addr_in.subscribe(self.update_addr_in)

        self.output: Wire = Wire()

    def update_data_in(self, new_sig: Signal) -> None:
        self.data_in = new_sig
        self.update()

    def update_addr_in(self, new_sig: Signal) -> None:
        self.addr_in = new_sig
        self.update()

    def set_wr(self, wr: bool) -> None:
        self.WR = wr
        self.update()

    def set_oe(self, oe: bool) -> None:
        self.OE = oe
        self.update()

    def set_cs(self, cs: bool) -> None:
        self.CS = cs
        self.update()

    def update(self) -> None:
        if self.CS:
            if self.WR or self.OE:
                assert not (self.WR and self.OE)
                if self.WR:
                    assert self.addr_in.value is not None
                    assert self.data_in.value is not None
                    self.data[self.addr_in.value] = self.data_in
                    self.output.set_signal(Signal(None))
                if self.OE:
                    assert self.addr_in.value is not None
                    self.output.set_signal(self.data[self.addr_in.value])
            else:
                self.output.set_signal(Signal(None))
        else:
            self.output.set_signal(Signal(None))


class OutputDevice:
    def __init__(self, data_in: Wire) -> None:
        self.CS: bool = False
        self.WR: bool = False

        self.data_in: Signal = Signal(None)
        data_in.subscribe(self.update_data_in)

        self.result: list[str] = []

    def update_data_in(self, new_sig: Signal) -> None:
        self.data_in = new_sig
        self.update()

    def set_cs(self, cs: bool) -> None:
        self.CS = cs
        self.update()

    def set_wr(self, wr: bool) -> None:
        self.WR = wr
        self.update()

    def update(self) -> None:
        if self.WR and self.CS:
            assert self.data_in.value is not None
            self.result.append(chr(self.data_in.value))
            logging.info(f"{chr(self.data_in.value)} --> OUTPUT {self.result}")


class InputDevice:
    def __init__(self, input_buffer: list[tuple[int, str]], clock: Wire) -> None:
        self.OE: bool = False
        self.CS: bool = False

        self.tick: Signal = Signal(None)
        clock.subscribe(self.update_tick)

        self.buffer: list[tuple[int, str]] = input_buffer
        self.next_char: int = 0
        self.next_trap: int = 0

        self.output: Wire = Wire()
        self.input_int: Wire = Wire()

    def update_tick(self, new_sig: Signal) -> None:
        assert new_sig.value is not None
        if 0 <= self.next_trap < len(self.buffer) and self.buffer[self.next_trap][0] <= new_sig.value:
            logging.info("TRAP GENERATED")
            self.input_int.set_signal(Signal(1))
            self.next_trap += 1
        self.tick = new_sig

    def set_oe(self, oe: bool) -> None:
        self.OE = oe
        self.update()

    def set_cs(self, cs: bool) -> None:
        self.CS = cs
        self.update()

    def update(self) -> None:
        if self.CS and self.OE:
            assert self.tick.value is not None
            assert self.buffer[self.next_char][0] <= self.tick.value
            self.output.set_signal(Signal(ord(self.buffer[self.next_char][1])))
            logging.info(f"{self.buffer[self.next_char][1]} <-- INPUT {self.buffer[self.next_char:]}")
            self.next_char += 1
        else:
            self.output.set_signal(Signal(None))


class AddressDecoder:
    def __init__(self, data_addr: Wire, memory: Memory, input_dev: InputDevice, output_dev: OutputDevice) -> None:
        self.data_addr: Signal = Signal(None)
        data_addr.subscribe(self.update_data_addr)

        self.memory = memory
        self.input_dev = input_dev
        self.output_dev = output_dev

    def update_data_addr(self, new_sig: Signal) -> None:
        self.data_addr = new_sig
        self.update()

    def update(self) -> None:
        if self.data_addr.value == IN_ADDR:
            self.input_dev.set_cs(True)
            self.output_dev.set_cs(False)
            self.memory.set_cs(False)
        elif self.data_addr.value == OUT_ADDR:
            self.input_dev.set_cs(False)
            self.output_dev.set_cs(True)
            self.memory.set_cs(False)
        else:
            self.input_dev.set_cs(False)
            self.output_dev.set_cs(False)
            self.memory.set_cs(True)


class RegisterFile:
    def __init__(self, reg_cnt: int, input_ireg: Wire) -> None:
        self.regs: list[Signal] = []
        for _ in range(reg_cnt):
            self.regs.append(Signal(None))

        self.input_ireg: Signal = Signal(None)
        input_ireg.subscribe(self.update_input_ireg)

        self.sel_ireg: int = 0
        self.sel_lreg: int = 0
        self.sel_rreg: int = 0

        self.out_lreg: Wire = Wire()
        self.out_rreg: Wire = Wire()

    def update_input_ireg(self, new_sig: Signal) -> None:
        self.input_ireg = new_sig

    def latch_ireg(self) -> None:
        self.regs[self.sel_ireg] = self.input_ireg
        self.update()

    def set_sel_ireg(self, sel: int) -> None:
        self.sel_ireg = sel

    def set_sel_lreg(self, sel: int) -> None:
        self.sel_lreg = sel
        self.update()

    def set_sel_rreg(self, sel: int) -> None:
        self.sel_rreg = sel
        self.update()

    def update(self) -> None:
        self.out_lreg.set_signal(self.regs[self.sel_lreg])
        self.out_rreg.set_signal(self.regs[self.sel_rreg])


@enum.unique
class AluOp(enum.Enum):
    PASS_R = enum.auto()
    PASS_L = enum.auto()
    INC_L = enum.auto()
    DEC_L = enum.auto()
    ADD = enum.auto()
    SUB = enum.auto()
    DIV = enum.auto()
    REM = enum.auto()
    CMP = enum.auto()


class ALU:
    def __init__(self, lhs_in: Wire, rhs_in: Wire) -> None:
        self.op: AluOp = AluOp.PASS_L
        self.lhs: Signal = Signal(None)
        self.rhs: Signal = Signal(None)

        self.N: bool = False
        self.Z: bool = False
        self.set_nz: bool = False

        self.alu_output: Wire = Wire()

        lhs_in.subscribe(self.update_l)
        rhs_in.subscribe(self.update_r)

    def update_single_arg(self) -> Signal:
        res: Signal = Signal(None)

        if self.op == AluOp.PASS_R:
            return self.rhs

        if self.lhs.value is None:
            return res

        match self.op:
            case AluOp.PASS_L:
                res = self.lhs
            case AluOp.INC_L:
                res = Signal(self.lhs.value + 1)
            case AluOp.DEC_L:
                res = Signal(self.lhs.value - 1)
            case _:
                pytest.fail("Unknown AluOp")

        return res

    def update_two_args(self) -> Signal:
        res: Signal = Signal(None)
        if self.lhs.value is None:
            return res
        if self.rhs.value is None:
            return res

        match self.op:
            case AluOp.ADD:
                res = Signal(self.lhs.value + self.rhs.value)
            case AluOp.SUB:
                res = Signal(self.lhs.value - self.rhs.value)
            case AluOp.DIV:
                if self.rhs.value == 0:
                    res = Signal(0xDEADBEEF)
                else:
                    res = Signal(self.lhs.value // self.rhs.value)
            case AluOp.REM:
                if self.rhs.value == 0:
                    res = Signal(0xDEADBEEF)
                    logging.info("REM RESULT DEADBEEF")
                else:
                    res = Signal(self.lhs.value % self.rhs.value)
                    logging.info(f"REM RESULT {self.lhs.value % self.rhs.value}")
            case AluOp.CMP:
                res = Signal(self.lhs.value - self.rhs.value)
            case _:
                pytest.fail("Unknown AluOp")

        return res

    def update(self) -> None:
        res: Signal = Signal(None)

        if self.op in [AluOp.PASS_L, AluOp.INC_L, AluOp.DEC_L]:
            res = self.update_single_arg()
        elif self.op in [AluOp.ADD, AluOp.SUB, AluOp.DIV, AluOp.REM, AluOp.CMP]:
            res = self.update_two_args()
        else:
            pytest.fail("Unknown AluOp")

        if res.value is not None and self.set_nz:
            self.N = res.value < 0
            self.Z = res.value == 0
            logging.info(f"SET N {self.N} Z {self.Z}")
            self.set_nz = False

        self.alu_output.set_signal(res)

    def update_l(self, new_lhs: Signal) -> None:
        self.lhs = new_lhs
        self.update()

    def update_r(self, new_rhs: Signal) -> None:
        self.rhs = new_rhs
        self.update()

    def set_op(self, new_op: AluOp) -> None:
        self.op = new_op
        self.update()

    def set_set_nz(self, val: bool) -> None:
        self.set_nz = val


@enum.unique
class DecOrIncMode(enum.Enum):
    DEC = enum.auto()
    INC = enum.auto()


class DecOrInc:
    def __init__(self, out_lreg: Wire) -> None:
        self.out_lreg: Signal = Signal(None)
        out_lreg.subscribe(self.update_out_lreg)
        self.mode: DecOrIncMode = DecOrIncMode.INC

        self.output: Wire = Wire()

    def update_out_lreg(self, new_sig: Signal) -> None:
        self.out_lreg = new_sig
        self.update()

    def set_mode(self, mode: DecOrIncMode) -> None:
        self.mode = mode
        self.update()

    def update(self) -> None:
        if self.out_lreg.value is None:
            self.output.set_signal(Signal(None))
        elif self.mode == DecOrIncMode.INC:
            self.output.set_signal(Signal(self.out_lreg.value + 1))
        else:
            self.output.set_signal(Signal(self.out_lreg.value - 1))


@enum.unique
class InputIReg(enum.Enum):
    DATA_REGISTER = enum.auto()
    CONTROL_UNIT_ARG = enum.auto()
    DEC_OR_INC = enum.auto()
    ALU_OUTPUT = enum.auto()


@enum.unique
class DataRegisterIn(enum.Enum):
    ALU_OUTPUT = enum.auto()
    DATA_OUT = enum.auto()
    PROGRAM_COUNT = enum.auto()


class DatapathOut(enum.Enum):
    ALU_OUTPUT = enum.auto()
    DATA_REGISTER = enum.auto()


class Datapath:
    def __init__(
        self,
        data_addr_in: Wire,
        control_unit_arg: Wire,
        program_count: Wire,
        initial_layout: dict[int, Signal],
        input_buffer: list[tuple[int, str]],
        clock: Wire,
    ) -> None:
        input_ireg_wire: Wire = Wire()

        self.register_file: RegisterFile = RegisterFile(11, input_ireg_wire)
        self.register_file.regs[SP_ID] = Signal(STACK_START)

        self.alu: ALU = ALU(self.register_file.out_lreg, self.register_file.out_rreg)

        self.dec_or_inc: DecOrInc = DecOrInc(self.register_file.out_lreg)

        self.AR: Latch = Latch(data_addr_in, "AR")

        data_in: Wire = Wire()
        self.memory: Memory = Memory(initial_layout, data_in, self.AR.output)
        self.output_dev: OutputDevice = OutputDevice(data_in)

        self.input_dev: InputDevice = InputDevice(input_buffer, clock)
        data_out: WireUnion = WireUnion(self.memory.output, self.input_dev.output)

        self.data_register_in_mux: Mux = Mux(
            [
                (DataRegisterIn.ALU_OUTPUT.value, self.alu.alu_output),
                (DataRegisterIn.DATA_OUT.value, data_out.output),
                (DataRegisterIn.PROGRAM_COUNT.value, program_count),
            ]
        )

        self.DR: Latch = Latch(self.data_register_in_mux.output, "DR")
        self.DR.output.subscribe(lambda x: data_in.set_signal(x))

        self.input_ireg_mux: Mux = Mux(
            [
                (InputIReg.ALU_OUTPUT.value, self.alu.alu_output),
                (InputIReg.DATA_REGISTER.value, self.DR.output),
                (InputIReg.CONTROL_UNIT_ARG.value, control_unit_arg),
                (InputIReg.DEC_OR_INC.value, self.dec_or_inc.output),
            ]
        )
        self.input_ireg_mux.output.subscribe(lambda x: input_ireg_wire.set_signal(x))

        self.IR: Latch = Latch(data_out.output, "IR")

        self.address_decoder: AddressDecoder = AddressDecoder(
            self.AR.output, self.memory, self.input_dev, self.output_dev
        )

        self.datapath_out_mux: Mux = Mux(
            [
                (DatapathOut.ALU_OUTPUT.value, self.alu.alu_output),
                (DatapathOut.DATA_REGISTER.value, self.DR.output),
            ]
        )

    def get_datapath_out(self) -> Wire:
        return self.datapath_out_mux.output

    def get_instruction(self) -> Wire:
        return self.IR.output

    def get_input_int(self) -> Wire:
        return self.input_dev.input_int

    def set_wr(self, wr: bool) -> None:
        self.memory.set_wr(wr)
        self.output_dev.set_wr(wr)

    def set_oe(self, oe: bool) -> None:
        self.memory.set_oe(oe)
        self.input_dev.set_oe(oe)
