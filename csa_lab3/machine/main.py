from __future__ import annotations

import logging
import sys

from csa_lab3.isa import Instruction
from csa_lab3.machine.control_unit import ControlUnit
from csa_lab3.machine.wire import Signal
from csa_lab3.mem_layout import INTERRUPT_VECTOR_0


def parse_isa_file(lines: list[str]) -> dict[int, Signal]:
    assert lines[0] == "START"
    assert lines[1] == "INT_0"
    assert lines[3] == "CODE"
    assert lines[-1] == "END"

    layout: dict[int, Signal] = dict()

    int_instr = Instruction.parse(lines[2])
    layout[INTERRUPT_VECTOR_0] = Signal(None, int_instr)

    for line in lines[4:-1]:
        addr, instr = line.split(maxsplit=1)
        layout[int(addr, 16)] = Signal(None, Instruction.parse(instr))

    return layout


def main(code_file: str, input_file: str) -> None:
    with open("out.log", "w+") as f:
        logging.basicConfig(stream=f, level=logging.DEBUG, format="%(message)s", filename="out.log", filemode="a")
        with open(code_file) as code:
            layout = parse_isa_file(list(map(lambda x: x.strip(), code.readlines())))
        with open(input_file) as inp:
            input_data = eval(inp.read())
        control_unit = ControlUnit(layout, input_data)
        try:
            while True:
                control_unit.cmd()
        except StopIteration:
            pass
        print("".join(control_unit.datapath.output_dev.result))


if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
