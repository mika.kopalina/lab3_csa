from __future__ import annotations

from io import TextIOBase
from typing import Iterable

from csa_lab3.isa import ArgType, Argument, Instruction, Opcode
from csa_lab3.mem_layout import (
    DATA_START,
    IN_ADDR,
    MAX_USER_REG,
    OUT_ADDR,
    TEXT_START,
)


def remove_comments_and_blank(src: Iterable[str]) -> Iterable[str]:
    return filter(lambda x: x, map(lambda x: x.partition(";")[0].strip(), src))


def examine_sections(clean_text: Iterable[str]) -> tuple[list[str], list[str], list[str]]:
    stage = -1
    names = [".interrupts", ".data", ".text"]
    sections: tuple[list[str], list[str], list[str]] = [], [], []

    for line in clean_text:
        before, section, name = line.partition("section")
        if section:
            stage += 1
            assert names[stage] == name.strip()
        else:
            assert 0 <= stage < len(sections)
            sections[stage].append(before)

    return sections


def translate_string(string: str, current_addr: int) -> tuple[list[Instruction], int]:
    res = []
    for c in string:
        res.append(Instruction(Opcode.MOV, [Argument(ArgType.REG, 1), Argument(ArgType.INT, ord(c))]))
        res.append(Instruction(Opcode.ST, [Argument(ArgType.INT, current_addr), Argument(ArgType.REG, 1)]))
        current_addr += 1

    res.append(Instruction(Opcode.MOV, [Argument(ArgType.REG, 1), Argument(ArgType.INT, 0)]))
    res.append(Instruction(Opcode.ST, [Argument(ArgType.INT, current_addr), Argument(ArgType.REG, 1)]))
    current_addr += 1
    return res, current_addr


def translate_word(value: str, current_addr: int) -> tuple[list[Instruction], int]:
    res = []
    if value.isdecimal():
        arg = Argument(ArgType.INT, int(value))
    elif value == "'\\n'":
        arg = Argument(ArgType.INT, ord("\n"))
    else:
        assert value[0] == "'"
        assert value[-1] == "'"
        assert len(value) == 3
        arg = Argument(ArgType.INT, ord(value[1]))
    res.append(Instruction(Opcode.MOV, [Argument(ArgType.REG, 1), arg]))
    res.append(Instruction(Opcode.ST, [Argument(ArgType.INT, current_addr), Argument(ArgType.REG, 1)]))

    return res, current_addr + 1


def translate_data_instr(instr: str, current_addr: int) -> tuple[list[Instruction], int]:
    words = instr.split(maxsplit=1)
    if words[0] == "cstr":
        string = words[1].strip()[1:-1]
        return translate_string(string, current_addr)
    if words[0] == "word":
        return translate_word(words[1].strip(), current_addr)
    words = instr.split(maxsplit=2)
    assert words[0].strip() == "times"
    assert words[1].strip().isdecimal()
    instrs: list[Instruction] = []
    for _ in range(int(words[1])):
        res, current_addr = translate_data_instr(words[2].strip(), current_addr)
        instrs.extend(res)
    return instrs, current_addr


def translate_data_section(data: Iterable[str]) -> tuple[list[Instruction], dict[str, int]]:
    instrs: list[Instruction] = []
    labels: dict[str, int] = dict()

    current_addr = DATA_START

    for line in data:
        before, colon, after = line.partition(":")
        assert not after
        if colon:
            labels[before.strip()] = current_addr
        else:
            res, current_addr = translate_data_instr(before.strip(), current_addr)
            instrs.extend(res)

    return instrs, labels


def partition_code_and_labels(text: Iterable[str], current_pos: int) -> tuple[list[str], dict[str, int]]:
    instrs: list[str] = []
    labels: dict[str, int] = dict()

    for line in text:
        before, colon, after = line.partition(":")
        assert not after
        if colon:
            labels[before.strip()] = current_pos
        else:
            current_pos += 1
            instrs.append(before)

    return instrs, labels


def translate_reg_arg(word: str) -> Argument:
    assert word[0] == "r"
    assert word[1:].isdecimal()
    assert int(word[1:]) <= MAX_USER_REG
    return Argument(ArgType.REG, int(word[1:]))


def translate_int_arg(word: str, labels: dict[str, int]) -> Argument:
    val: int
    if word.isdecimal():
        val = int(word)
    elif word[0] == "'":
        if word == "'\\n'":
            val = ord("\n")
        else:
            assert word[2] == "'"
            val = ord(word[1])
    else:
        assert word in labels, f"Unkown label: {word}"
        val = labels[word]

    return Argument(ArgType.INT, val)


def translate_reg_or_int_arg(word: str, labels: dict[str, int]) -> Argument:
    if word[0] == "r":
        return translate_reg_arg(word)
    return translate_int_arg(word, labels)


def translate_mem_instr(words: list[str], data_labels: dict[str, int]) -> Instruction:
    opcode = Opcode[words[0]]
    if opcode == Opcode.MOV:
        assert len(words) == 3
        return Instruction(opcode, [translate_reg_arg(words[1]), translate_reg_or_int_arg(words[2], data_labels)])
    if opcode == Opcode.LD:
        assert len(words) == 3
        return Instruction(opcode, [translate_reg_arg(words[1]), translate_reg_or_int_arg(words[2], data_labels)])
    assert opcode == Opcode.ST
    assert len(words) == 3
    return Instruction(opcode, [translate_reg_or_int_arg(words[1], data_labels), translate_reg_arg(words[2])])


def translate_instruction(line: str, data_labels: dict[str, int], text_labels: dict[str, int]) -> Instruction:
    words = list(map(lambda x: x.strip(), line.split()))
    opcode = Opcode[words[0]]

    if opcode in [Opcode.HALT, Opcode.NOP, Opcode.EI, Opcode.DI, Opcode.RET]:
        assert len(words) == 1
        return Instruction(opcode, [])

    if opcode == Opcode.CALL:
        assert len(words) == 2
        return Instruction(opcode, [translate_int_arg(words[1], text_labels)])

    if opcode in [Opcode.INC, Opcode.DEC, Opcode.PUSH, Opcode.POP]:
        assert len(words) == 2
        return Instruction(opcode, [translate_reg_arg(words[1])])

    if opcode in [Opcode.JMP, Opcode.JG, Opcode.JZ, Opcode.JNZ]:
        assert len(words) == 2
        return Instruction(opcode, [translate_reg_or_int_arg(words[1], text_labels)])

    if opcode == Opcode.CMP:
        assert len(words) == 3
        return Instruction(opcode, [translate_reg_arg(words[1]), translate_reg_arg(words[2])])

    if opcode in [Opcode.ADD, Opcode.SUB, Opcode.DIV, Opcode.REM, Opcode.MUL]:
        assert len(words) == 4
        return Instruction(
            opcode,
            [translate_reg_arg(words[1]), translate_reg_arg(words[2]), translate_reg_arg(words[3])],
        )

    assert opcode in [Opcode.MOV, Opcode.LD, Opcode.ST]
    return translate_mem_instr(words, data_labels)


def translate(src: TextIOBase) -> tuple[Instruction, list[Instruction]]:
    clean_text = remove_comments_and_blank(src)
    interrupts, data, text = examine_sections(clean_text)

    data_instrs, data_labels = translate_data_section(data)
    data_labels["IN"] = IN_ADDR
    data_labels["OUT"] = OUT_ADDR

    text_code, text_labels = partition_code_and_labels(text, TEXT_START + len(data_instrs) + 1)

    text_instrs = list(
        map(
            lambda x: translate_instruction(x, data_labels, text_labels),
            ["JMP _start", *text_code],
        )
    )

    int_instr = translate_instruction(interrupts[1], data_labels, text_labels)

    return (int_instr, data_instrs + text_instrs)
