import sys

from csa_lab3.translator.translator import TEXT_START, translate


def main(src: str, dst: str) -> None:
    with open(src) as fin:
        int_instr, instrs = translate(fin)
    with open(dst, "w+") as fout:
        print("START", file=fout)
        print("INT_0", file=fout)
        print(int_instr, file=fout)
        print("CODE", file=fout)
        for i, instr in enumerate(instrs):
            print(f"{hex(i+TEXT_START):<10} {instr}", file=fout)
        print("END", file=fout)


if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
